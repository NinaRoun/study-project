import React from 'react'

const Loading = () => (
    <div>
        <strong>Loading...</strong>
    </div>
)

export default Loading
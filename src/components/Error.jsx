import React from 'react'

const Error = ({error}) => (
    <div>
        <strong>{ error }</strong>
    </div>
)

export default Error